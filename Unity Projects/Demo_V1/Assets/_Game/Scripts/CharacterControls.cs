﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class CharacterControls : MonoBehaviour
{
    [HideInInspector]
    public Rigidbody rb;

    [Header("Movement Variables")]
    public float moveSpeed = 10.0f;
    public float gravity = 10.0f;
    public float maxVelocityChange = 10.0f;

    [Header("Jumping Variables")]
    public bool canJump = true;
    public float jumpHeight = 2.0f;

    private bool grounded = false;

    [Header("Animation variables")]
    public Animator animator;

    [HideInInspector]
    public bool inDialogue = false;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();

        rb.freezeRotation = true;
        rb.useGravity = false;
    }

    void FixedUpdate()
    {


        if (!inDialogue)
        {
            if (grounded)
            {
                // Calculate how fast we should be moving
                Vector3 targetVelocity = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
                targetVelocity = Camera.main.transform.TransformDirection(targetVelocity);
                targetVelocity *= moveSpeed;

                // Apply a force that attempts to reach our target velocity
                Vector3 velocity = rb.velocity;
                Vector3 velocityChange = (targetVelocity - velocity);
                velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
                velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
                velocityChange.y = 0;
                rb.AddForce(velocityChange, ForceMode.VelocityChange);


            }

            // We apply gravity manually for more tuning control
            rb.AddForce(new Vector3(0, -gravity * rb.mass, 0));
        }

        //Check for animations
        if (grounded)
        {
            Vector3 newVelocity = rb.velocity;
            newVelocity.y = 0;

            animator.SetFloat("Movement Speed", newVelocity.magnitude);
        }

        if (!inDialogue)
        {
            Vector3 lookAtTarget = transform.position + rb.velocity;
            lookAtTarget.y = transform.position.y;

            transform.LookAt(lookAtTarget);
        }
        grounded = false;
    }

    private void Update()
    {
        // Jump
        if (!inDialogue && canJump && Input.GetButtonDown("Jump"))
        {
            Vector3 velocity = rb.velocity;

            rb.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);

            //Debug.Log("Called");
            if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Jump"))
                animator.SetTrigger("Jump");
        }
    }


    void OnCollisionStay()
    {
        grounded = true;
    }

    float CalculateJumpVerticalSpeed()
    {
        // From the jump height and gravity we deduce the upwards speed 
        // for the character to reach at the apex.
        return Mathf.Sqrt(2 * jumpHeight * gravity);
    }
}