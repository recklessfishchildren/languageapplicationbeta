﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class WordArrange_Slot : MonoBehaviour, IDropHandler
{

    public void OnDrop(PointerEventData eventData)
    {
        //Debug.Log("Dropped on " + gameObject.name);
        WordArrange_DragWord.itemBeingDragged.transform.SetParent(transform);

    }
}
