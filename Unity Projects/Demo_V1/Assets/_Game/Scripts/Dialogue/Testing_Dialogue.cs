﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// This script goes on the NPC. It controls the NPC's Dialogue
/// </summary>
public class Testing_Dialogue : MonoBehaviour {

    [Tooltip("The dialogue this character will do")]
    public Convo_Dialogue npcDialogue;
    public KeyCode nextKey = KeyCode.Space;

    NPC_Dialogue dialogueController;

    int currentNodeID;

    int npcComment;
    int optionChosen = 0;
    string ghostString;

    Convo_Comment wordArrange_currentComment;
    string wordArrange_correctSentance;

    bool dialogueStarted = false;
    bool wordArrangeStarted = false;

    bool waitingOnPlayerButtonPress = false;
    bool endDialogue = false;


    void Start()
    {
        InitializeDialogue();
    }

    void InitializeDialogue()
    {
        Testing_DialogueUI.instance.HideDialogue();
        Testing_DialogueUI.instance.HidePlayerBox();
        currentNodeID = npcDialogue.startNode;
    }

    public void StartLoadedDialogue()
    {
        if (!dialogueStarted)
        {
            Testing_DialogueUI.instance.ShowDialogue();
            dialogueStarted = true;

            dialogueController = GetComponent<NPC_Dialogue>();
            if(dialogueController.onlyOnce)
            {
                dialogueController.DestroyTrigger();
            }

            Testing_DialogueUI.instance.ResetGhost();
            ShowLine();
        }
    }

    void ResetEngine()
    {
        dialogueStarted = false;
        Testing_DialogueUI.instance.HideDialogue();
        GetComponent<NPC_Dialogue>().DeactivateScene();

        currentNodeID = npcDialogue.startNode;
        endDialogue = false;
        npcComment = 0;
        optionChosen = 0;
    }

    void ShowLine()
    {
        if (endDialogue == true)
        {
            ResetEngine();
            return;
        }

        Convo_Node node = GetCurrentNode();
        ghostString = "";

        if (node.isPlayer)
            PlayerNode(node);
        else
            NPCNode(node);
    }

    void PlayerNode(Convo_Node node)
    {
        Testing_DialogueUI.instance.ShowPlayerBox();
        Testing_DialogueUI.instance.HideNPCBox();

        Testing_DialogueUI.instance.UpdateName("Player");

        waitingOnPlayerButtonPress = true;

        //Foreach comment in the player node
        for(int i = 0; i < node.comments.Count; i++)
        {
            //Create a button
            Button tempButton = Testing_DialogueUI.instance.CreateResponse();
            Text tempText = tempButton.GetComponentInChildren<Text>();

            string sentance = "";
            foreach (Convo_Word word in node.comments[i].sentance)
            {
                if (!node.comments[i].displayNative)
                {
                    if (word.nativeNoSpace)
                    {
                        sentance += word.native;
                        ghostString += word.native;
                    }
                    else
                    {
                        sentance += word.native + " ";
                        ghostString += word.name + " ";
                    }
                }
                else
                {
                    if (word.foreignNoSpace)
                    {
                        sentance += word.foreign;
                        ghostString += word.foreign;
                    }
                    else
                    {
                        sentance += word.foreign + " ";
                        ghostString += word.foreign + " ";
                    }
                }
            }

            //Display the button text
            tempText.text = sentance;
            int tempInt = i;
            tempButton.onClick.AddListener(delegate { _SelectOption(tempInt); });


        }

        //Show the player response panel (with the buttons and everything)
        Testing_DialogueUI.instance.playerResponsePanel.SetActive(true);

    }

    void NPCNode(Convo_Node node)
    {
        Testing_DialogueUI.instance.UpdateName("NPC");

        Testing_DialogueUI.instance.HidePlayerBox();
        Testing_DialogueUI.instance.ShowNPCBox();

        string sentance = "";
        ghostString = "";

        //Create the sentance
        foreach (Convo_Word word in node.comments[npcComment].sentance)
        {
            if (node.comments[npcComment].displayNative)
            {
                if (word.nativeNoSpace)
                {
                    sentance += word.native;
                    ghostString += word.native;
                }
                else
                {
                    sentance += word.native + " ";
                    ghostString += word.native + " ";
                }
            }
            else
            {
                if (word.foreignNoSpace)
                {
                    sentance += word.foreign;
                    ghostString += word.foreign;
                }
                else
                {
                    sentance += word.foreign + " ";
                    ghostString += word.foreign + " ";
                }
            }
        }

        //Show the dialogue
        Testing_DialogueUI.instance.UpdateDialogueText(sentance);

        //Add the ghost string
        Testing_DialogueUI.instance.AddToGhost(ghostString, false);

        if (npcComment >= node.comments.Count - 1)
        {
            if (GoToNextNode(npcComment) == -1)
            {
                endDialogue = true;
            }

            npcComment = 0;
        }
        else
            npcComment++;
    }

    void _SelectOption(int optionNumber)
    {
        
        optionChosen = optionNumber;

        Testing_DialogueUI.instance.HidePlayerBox();
        Testing_DialogueUI.instance.CleanupPlayerBox();
        WordArrangeSetup(GetCurrentNode().comments[optionNumber]);

        waitingOnPlayerButtonPress = false;

    }

    //Adds all the buttons and everything to the elements
    void WordArrangeSetup(Convo_Comment comment)
    {
        wordArrangeStarted = true;

        wordArrange_currentComment = comment;

        List<Convo_Word> shuffledWords = ShuffleList(comment);

        wordArrange_correctSentance = "";

        foreach(Convo_Word word in shuffledWords)
        {
            GameObject tempObj = Instantiate(Testing_DialogueUI.instance.wordArrange_buttonTemplate, Testing_DialogueUI.instance.notArranged.transform);
            tempObj.GetComponent<WordArrange_DragWord>().dragParent = Testing_DialogueUI.instance.dragParent.transform;

            Text tempText = tempObj.GetComponentInChildren<Text>();
            tempText.text = word.foreign;
        }

        string nativeSentance = "";

        foreach(Convo_Word word in comment.sentance)
        {
            wordArrange_correctSentance += word.foreign + " ";
            nativeSentance += word.native + " ";
        }

        Testing_DialogueUI.instance.UpdateWordArrangeCorrectSentance(nativeSentance);


        StartCoroutine(ResizeObject());

        Testing_DialogueUI.instance.ShowWordArrange();
    }

    void WordArrangeCheckSentance()
    {
        string submittedSentance = "";

        foreach(Transform child in Testing_DialogueUI.instance.arranged.transform)
        {
            submittedSentance += child.GetComponentInChildren<Text>().text + " ";
        }


        if(wordArrange_correctSentance == submittedSentance)
        {
            Testing_DialogueUI.instance.WordArrangeCleanUp();

            if (GoToNextNode(optionChosen) == -1)
                Debug.Log("Close the dialogue, player ended");
            else
                ShowLine();

            //Add the Ghost String
            Testing_DialogueUI.instance.AddToGhost(submittedSentance, true);

            wordArrangeStarted = false;
            //Hide the word arrange, 
            Testing_DialogueUI.instance.HideWordArrange();
        }
        else
        {
            //Need to get buttons and their text so we can colour them
            List<Button> buttonList = new List<Button>();
            List<string> arrangedSentance = new List<string>();

            foreach(Transform child in Testing_DialogueUI.instance.arranged.transform)
            {
                buttonList.Add(child.GetComponent<Button>());
                arrangedSentance.Add(child.GetComponentInChildren<Text>().text);
            }

            List<string> listCorrectSentance = new List<string>(wordArrange_correctSentance.Split(' '));

            for(int i = 0; i < arrangedSentance.Count; i++)
            {
                if (arrangedSentance[i].Replace(" ", string.Empty) != listCorrectSentance[i].Replace(" ", string.Empty))
                {
                    ColorBlock colBlock = buttonList[i].colors;
                    colBlock.normalColor = Color.red;
                    buttonList[i].colors = colBlock;
                }
            }
        }
    }

    IEnumerator ResizeObject()
    {
        yield return new WaitForEndOfFrame();

        foreach (Transform word in Testing_DialogueUI.instance.notArranged.transform)
        {
            Text tempTxt = word.GetComponentInChildren<Text>();
            float size = tempTxt.preferredWidth;

            LayoutElement tempElement = word.GetComponent<LayoutElement>();
            tempElement.preferredWidth = size + Testing_DialogueUI.instance.buffer;
        }
    }

    List<Convo_Word> ShuffleList(Convo_Comment comment)
    {
        List<Convo_Word> inputWords = new List<Convo_Word>(comment.sentance);
        List<Convo_Word> randomList = new List<Convo_Word>();

        int randomIndex = 0;

        while(inputWords.Count > 0)
        {
            randomIndex = Random.Range(0, inputWords.Count);
            randomList.Add(inputWords[randomIndex]);
            inputWords.RemoveAt(randomIndex);
        }

        return randomList;
    }

    private void Update()
    {
        if(dialogueStarted && !wordArrangeStarted && !waitingOnPlayerButtonPress && Input.GetKeyDown(nextKey))
        {
            ShowLine();
        }

        if(wordArrangeStarted && Input.GetKeyDown(KeyCode.C))
        {
            WordArrangeCheckSentance();
        }
    }

    #region Control Information

    /// <summary>
    /// Get the next node ID.
    /// Does not move the system to the next node
    /// </summary>
    /// <param name="chosenOption"></param>
    /// <returns>The next node ID</returns>
    public int NextNode(int chosenOption = 0)
    {
        return GetNodeByID(currentNodeID).comments[chosenOption].outNode;
    }

    /// <summary>
    /// Moves the system to the next node
    /// </summary>
    /// <param name="chosenOption"></param>
    /// <returns>The new node. -1 means its done</returns>
    public int GoToNextNode(int chosenOption = 0)
    {
        int nextNode = -1;

        nextNode = GetNodeByID(currentNodeID).comments[chosenOption].outNode;

        if (nextNode == -1)
        {
            //Debug.Log("END OF THE LINE");

            return -1;
        }
        else
        {
            currentNodeID = nextNode;
            return nextNode;
        }
    }

    /// <summary>
    /// Gets the current node's ID
    /// </summary>
    /// <returns>current node ID</returns>
    public int CurrentNode()
    {
        return currentNodeID;
    }

    /// <summary>
    /// Gets the current node
    /// </summary>
    /// <returns>Current node</returns>
    public Convo_Node GetCurrentNode()
    {
        return GetNodeByID(currentNodeID);
    }

    /// <summary>
    /// Gets a node by it's Index
    /// </summary>
    /// <param name="index">The index of the node to get</param>
    /// <returns>A node at index [index]</returns>
    public Convo_Node GetNodeByIndex(int index)
    {
        return npcDialogue.nodes[index];
    }

    /// <summary>
    /// Gets a node by it's ID
    /// </summary>
    /// <param name="id">the ID of the node to get</param>
    /// <returns>A node with the ID [id]</returns>
    public Convo_Node GetNodeByID(int id)
    {
        foreach (Convo_Node node in npcDialogue.nodes)
        {
            if (node.nodeID == id)
                return node;
        }

        return null;
    }
    #endregion
}
