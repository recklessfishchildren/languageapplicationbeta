﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Convo_Node {

    public int nodeID;
    public bool isPlayer;
    public List<Convo_Comment> comments;
    public Convo_NodeGUI gui;

    public GameObject nodeModel;
    public GameObject nodePosition;

    public Convo_Node()
    {
        gui = new Convo_NodeGUI();
        comments = new List<Convo_Comment>();
        comments.Add(new Convo_Comment());
    }
}
