﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable, CreateAssetMenu(fileName = "New Dialogue", menuName = "Dialogue/New Dialogue", order = 0)]
public class Convo_Dialogue : ScriptableObject {

    public List<Convo_Node> nodes;
    public int startNode = 0;
    public string dialogueName;

    public GameObject npcModel;
    public GameObject pcModel;
}
