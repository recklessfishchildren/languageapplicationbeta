﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[Serializable]
public class Convo_NodeGUI
{
    public float posX;
    public float posY;
    public float width;
    public float height;

    public string title;

    public bool isDragged;

    public Rect[] handleRects;

    public Rect GetRect()
    {
        return new Rect(posX, posY, width, height);
    }

    public void SetRect(float posX, float posY, float width, float height)
    {
        this.posX = posX;
        this.posY = posY;
        this.width = width;
        this.height = height;
    }

    public void SetRect(Rect newRect)
    {
        SetRect(newRect.position.x, newRect.position.y, newRect.width, newRect.height);
    }

    public void Drag(Vector2 delta)
    {
        Rect newRect = GetRect();
        newRect.position += delta;
        SetRect(newRect);
    }
}
