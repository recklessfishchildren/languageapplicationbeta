﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Testing_Dialogue))]
public class NPC_Dialogue : MonoBehaviour {

    CharacterDollySwap dollySwap;
    CharacterControls charControls;

    bool inArea = false;

    [Header("Scene Setting")]
    [Tooltip("Place the Cinemachine camera that will be used for this shot here")]
    public Cinemachine.CinemachineVirtualCamera dialogueCamera;
    Cinemachine.CinemachineVirtualCamera originalCamera;

    [Tooltip("Would you like to player to snap to a position when the dialogue is played?")]
    public bool teleportPlayer;
    [Tooltip("The location the player will snap to (This also controls the players rotation!")]
    public Transform scenePlayerTransform;

    [Header("Interact Settings")]
    public KeyCode interactKey = KeyCode.Space;
    public bool onlyOnce = false;

    [Header("Testing")]
    [Tooltip("If this is true, you do not have to include any character/camera info. Just interaction info.")]
    public bool isTesting = false;

    Transform originalTransform;
    Transform playerTransform;

    Testing_Dialogue dialogueController;

    private void Start()
    {
        if (isTesting)
        {
            dialogueController = GetComponent<Testing_Dialogue>();
            return;
        }

        playerTransform = GameObject.FindWithTag("Player").transform;
        dollySwap = playerTransform.gameObject.GetComponent<CharacterDollySwap>();
        charControls = playerTransform.gameObject.GetComponent<CharacterControls>();
        dialogueController = GetComponent<Testing_Dialogue>();

    }

    private void Update()
    {
        if(isTesting)
        {
            if (Input.GetKeyDown(interactKey))
            {
                ActivateScene();
                return;
            }
        }

        if (Input.GetKeyDown(interactKey) && inArea)
        {
            ActivateScene();
        }

    }

    void ActivateScene()
    {
        if(isTesting)
        {
            dialogueController.StartLoadedDialogue();
            return;
        }

        charControls.inDialogue = true;
        charControls.rb.velocity = Vector3.zero;

        if (teleportPlayer)
        {
            originalTransform = playerTransform.transform;
            playerTransform.position = scenePlayerTransform.position;
            playerTransform.rotation = scenePlayerTransform.rotation;
        }
            originalCamera = dollySwap.currentCamera;
            dollySwap.ExternalSwap(dialogueCamera);

        dialogueController.StartLoadedDialogue();
    }

    public void DeactivateScene()
    {
        if (isTesting)
            return;
        charControls.inDialogue = false;

        if (teleportPlayer)
        {
            playerTransform.position = originalTransform.position;
            playerTransform.rotation = originalTransform.rotation;
        }

        dollySwap.ExternalSwap(originalCamera);
    }

    public void DestroyTrigger()
    {
        GetComponent<BoxCollider>().enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
            inArea = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
            inArea = false;

    }
}
