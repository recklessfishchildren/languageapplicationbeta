﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using bf = System.Reflection.BindingFlags;
using System.Text.RegularExpressions;
using System.Linq;
using System;

public class Dialogue_Editor : EditorWindow
{
    /// <summary>
    /// The entire dialogue screen
    /// </summary>
    //Rect dialogueBox;

    Convo_Dialogue currentDialogue;
    Convo_Node currentSelected;

    Texture2D selectedTexture;
    Texture2D pcTexture;
    Texture2D npcTexture;
    Texture2D lineIcon;

    Color defaultColour;

    //GUIStyle conversationNode;
    GUIStyle currentNodeStyle;

    //Drawing variables
    bool isDrawingLine;
    bool isDragging;
    Color32[] colors;

    //Drawing the Handles and Line connections
    Rect handleRect;
    int handleCommentNumber;
    Convo_NodeGUI handleNodeGUI;

    //Event
    Event mouseEvent;

    [MenuItem("Conversation Editor/Open")]
    static void ShowEditor()
    {
        Dialogue_Editor editor = GetWindow<Dialogue_Editor>();
        editor.Init();
    }

    void Init()
    {
        Debug.Log("Initializing the dialogue...");

        //Get the dialogue that is being edited
        //currentDialogue = GameObject.Find("DialogueEditor").GetComponent<DialogueTester>().currentDialogue;
        //currentDialogue = new Convo_Dialogue();
        currentDialogue = null;

        //Title
        //GUIContent titleContent = new GUIContent(currentDialogue.dialogueName);
        //GetWindow<Dialogue_Editor>().titleContent = titleContent;

        //Reference to the Dialogue window
        Dialogue_Editor editor = GetWindow<Dialogue_Editor>();
        editor.position = new Rect(50, 50, 1027, 768);


        //Load the textures that will be used
        //6 = red, 5 = orange, 4 = yellow, 3 = green, 2 = teal, 1 = blue
        selectedTexture = EditorGUIUtility.Load("builtin skins/darkskin/images/node4.png") as Texture2D;
        pcTexture = EditorGUIUtility.Load("builtin skins/darkskin/images/node1.png") as Texture2D;
        npcTexture = EditorGUIUtility.Load("builtin skins/darkskin/images/node3.png") as Texture2D;

        //What the node is going to look like when it is drawn (background color, etc.)
        currentNodeStyle = new GUIStyle();
        currentNodeStyle.border = new RectOffset(12, 12, 12, 12);


        //The little arrow that points between the items
        lineIcon = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Editor/lineIcon.png", typeof(Texture2D));

        ////Set colors we'll be using later (mainly for drawing lines and arrows)
        colors = new Color32[]{new Color32(255,255,255,255),
            new Color32(180,180,180,255),
            new Color32(142,172,180,255),
            new Color32(84,110,137,255),
            new Color32(198,143,137,255)
        };

    }
    Vector2 scrollPosition;

    private void OnGUI()
    {
        scrollPosition = GUI.BeginScrollView(new Rect(0, 0, position.width, position.height), scrollPosition, new Rect(0, 0, 5000, 5000));

        Event e = Event.current;
        mouseEvent = e;

        //color for GUI
        defaultColour = GUI.color;

        if (currentDialogue != null)
        {
            DrawTopToolbar();
            DrawNodes();
            DrawLines();

            ProcessEvents(Event.current);
        }
        else
        {
            currentDialogue = (Convo_Dialogue)EditorGUILayout.ObjectField("Editing Dialogue: ", currentDialogue, typeof(Convo_Dialogue), false, GUILayout.Width(250));
        }
        GUI.EndScrollView();
    }

    Vector2 _lastMousePos = new Vector2(-999, -999);

    void ProcessEvents(Event e)
    {
        if(isDrawingLine)
        {
            if(e.type == EventType.MouseDown)
            {
                isDrawingLine = false;
                e.Use();
            }

            TryConnectNode(e.mousePosition);
        }

        switch (e.type)
        {
            case EventType.MouseDown:
                if (e.button == 1) // right click
                    ProcessContextMenu(e.mousePosition);
                else if (e.button == 0) //left click
                {
                    if(isDrawingLine)
                    {

                    }
                    if (FindOver(e.mousePosition) != null)
                    {
                        currentSelected = FindOver(e.mousePosition);
                        isDragging = true;
                        GUI.changed = true;
                    }
                    else
                        currentSelected = null;
                }
                break;
                
            case EventType.MouseDrag:
                if (e.button == 0 && isDragging)
                {
                    currentSelected.gui.Drag(e.delta);
                    e.Use();
                }
                else if(e.button == 2)
                {
                    Vector2 currPos = e.mousePosition;

                    if(Vector2.Distance(currPos, _lastMousePos) < 5)
                    {
                        float x = _lastMousePos.x - currPos.x;
                        float y = _lastMousePos.y - currPos.y;

                        scrollPosition.x += x;
                        scrollPosition.y += y;
                        e.Use();
                    }

                    _lastMousePos = currPos;
                }
                break;
            case EventType.MouseUp:
                isDragging = false;
                break;
        }
    }

    void ProcessContextMenu(Vector2 mousePosition)
    {
        GenericMenu genericMenu = new GenericMenu();
        genericMenu.AddItem(new GUIContent("Add node"), false, () => OnClickAddNode(mousePosition));
        genericMenu.ShowAsContext();
    }

    void OnClickAddNode(Vector2 mousePosition)
    {
        if (currentDialogue.nodes == null)
            currentDialogue.nodes = new List<Convo_Node>();

        //Add a node to the list
        Convo_Node tempNode = new Convo_Node();
        tempNode.gui.SetRect(mousePosition.x, mousePosition.y, 200, 150);
        tempNode.nodeID = GetUniqueID();

        currentDialogue.nodes.Add(tempNode);

        Repaint();
    }

    void DrawTopToolbar()
    {
        GUIStyle toolbarStyle = new GUIStyle(EditorStyles.toolbar);

        GUILayout.Space(scrollPosition.y+5f);
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("", GUILayout.Width(scrollPosition.x));
        EditorGUIUtility.labelWidth = 100f;

        string currName = currentDialogue.dialogueName;
        currentDialogue.dialogueName = EditorGUILayout.TextField("Dialogue Name: ", currentDialogue.dialogueName, GUILayout.Width(250));

        if (currentDialogue.dialogueName != currName)
        {
            GetWindow<Dialogue_Editor>().titleContent = new GUIContent(currentDialogue.dialogueName);

            string assetPath = AssetDatabase.GetAssetPath(currentDialogue.GetInstanceID());
            AssetDatabase.RenameAsset(assetPath, currentDialogue.dialogueName);
            AssetDatabase.SaveAssets();
        }
        //if (GUILayout.Button("Save Dialogue", GUILayout.MaxWidth(100)))
        //{
        //    string json = EditorJsonUtility.ToJson(currentDialogue);

        //    if (currentDialogue.dialogueName != null)
        //    {
        //        //Trying with just SO's
        //        EditorUtility.SetDirty(currentDialogue);
        //        Debug.Log("Saved!");

        //    }
        //    else
        //    {
        //        Debug.Log("You need a real dialogue name");
        //    }
        //}
        if (currentDialogue == null)
        {
            Debug.Log("Null!");
        }

        //TESTING
        currentDialogue = (Convo_Dialogue)EditorGUILayout.ObjectField("Editing Dialogue: ", currentDialogue, typeof(Convo_Dialogue), false, GUILayout.Width(250));
        
        //Showing NPC and PC Models
        //EditorGUIUtility.labelWidth = 75f;
        //currentDialogue.npcModel = (GameObject)EditorGUILayout.ObjectField("NPC Model: ", currentDialogue.npcModel, typeof(GameObject), false, GUILayout.Width(150));

        //EditorGUILayout.Space();

        //EditorGUIUtility.labelWidth = 65;
        //currentDialogue.pcModel = (GameObject)EditorGUILayout.ObjectField("PC Model: ", currentDialogue.pcModel, typeof(GameObject), false, GUILayout.Width(150));

        EditorGUIUtility.labelWidth = 70;
        currentDialogue.startNode = EditorGUILayout.IntField("Start Node: ",currentDialogue.startNode, GUILayout.Width(125));

        EditorGUILayout.EndHorizontal();
    }

    void DrawNodes()
    {
        if (currentDialogue.nodes != null)
        {
            //Draw each node
            foreach(Convo_Node node in currentDialogue.nodes)
            {
                if (node.isPlayer)
                    currentNodeStyle.normal.background = pcTexture;
                else
                    currentNodeStyle.normal.background = npcTexture;

                //Pick the node colour
                if (currentSelected != null)
                {
                    if (currentSelected == node)
                        currentNodeStyle.normal.background = selectedTexture;
                }

                //Draw the node box
                GUI.Box(node.gui.GetRect(), node.gui.title, currentNodeStyle);

                DrawInsideNode(node);
            }

            GUILayout.BeginArea(new Rect(scrollPosition.x + 7, scrollPosition.y + 25, 500, 20));
            //Draw the top toolbar for deleting a node and adding a comment to a node
            EditorGUILayout.BeginHorizontal();
            //EditorGUILayout.LabelField("", GUILayout.Width(scrollPosition.y));  

            GUIStyle toolbarStyle = new GUIStyle(EditorStyles.toolbar);

            if (GUILayout.Button("Delete Node?", GUILayout.Width(100)))
            {
                //Delete the node
                if (!DeleteNode(currentSelected))
                    Debug.Log("Something went wrong with deleting the node...");
            }

            if(GUILayout.Button("Add comment", GUILayout.Width(140)))
            {
                //Add a comment
                currentSelected.comments.Add(new Convo_Comment());
            }

            EditorGUILayout.EndHorizontal();
            GUILayout.EndArea();
            //Draw the bottom toolbar if a node is selected
            if(currentSelected != null)
                DrawNodeEditor();
        }
        else
            currentDialogue.nodes = new List<Convo_Node>();

        Repaint();
    }

    void DrawInsideNode(Convo_Node node)
    {
        Rect nodePosition = node.gui.GetRect();
        Rect currentPosition = nodePosition;

        
        currentPosition.y += 5f;

        if (node.isPlayer)
        {
            currentPosition.x += 40f;
            GUI.Label(currentPosition, "Player Node ID: " + node.nodeID, EditorStyles.boldLabel);
        }
        else
        {
            currentPosition.x += 50f;
            GUI.Label(currentPosition, "NPC Node ID: " + node.nodeID, EditorStyles.boldLabel);
        }

        if(node.comments.Count > 0)
        {
            currentPosition = node.gui.GetRect();
            currentPosition.x += 10f;
            currentPosition.y += 10f;

            int commentNumber = 0;
            foreach(Convo_Comment comment in node.comments)
            {
                string sentance = "";

                foreach(Convo_Word word in comment.sentance)
                {
                    if (node.isPlayer)
                    {
                        if (comment.displayNative)
                            sentance += word.foreign;
                        else
                            sentance += word.native;
                    }
                    else
                    {
                        if (comment.displayNative)
                            sentance += word.native;
                        else
                            sentance += word.foreign;
                    }

                    if (!word.nativeNoSpace)
                        sentance += " ";
                }

                currentPosition.y += 15f;

                if(sentance.Length > 28)
                {
                    sentance = sentance.Substring(0, 25);
                    sentance += "...";
                }

                GUI.Label(currentPosition, sentance);

                if(node == currentSelected)
                    DrawLineHandle(currentPosition, Color.yellow, commentNumber, node.gui);
                else if (node.isPlayer)
                    DrawLineHandle(currentPosition, Color.blue, commentNumber, node.gui);
                else
                    DrawLineHandle(currentPosition, Color.green, commentNumber, node.gui);

                currentPosition.y += 5f;
                commentNumber++;
            }
        }
    }

    void DrawLineHandle(Rect commentPosition, Color buttonColor, int commentNum, Convo_NodeGUI nodeGUI)
    {
        Rect handlePos = commentPosition;
        handlePos.x += 180f;
        handlePos.width = 10f;
        handlePos.height = 18f;

        GUI.backgroundColor = buttonColor;

        if(GUI.Button(handlePos, GUIContent.none))
        {
            isDrawingLine = true;
            handleRect = handlePos;
            handleCommentNumber = commentNum;
            handleNodeGUI = nodeGUI;
            currentSelected = FindByHandle(handlePos);
        }

        GUI.backgroundColor = defaultColour;
    }

    void DrawNodeEditor()
    {
        //Bottom 35% of the window will be the node editor
        float heightOfWindow = position.height * 0.35f - 20;
        //float heightOfWindow = editor.scrollPosition.y;


        float widthOfWindow = position.width - 30f;
        //float widthOfWindow = 1000;

        float startPointY = position.height - (position.height * 0.35f) + scrollPosition.y;
        //float startPointY = position.height;// - scrollPosition.y;

        //GUI.Box(new Rect(editor.scrollPosition.x, editor.scrollPosition.y, position.width, position.height), GUIContent.none);
        //Debug.Log(scrollPosition);
        

        Rect editorBox = new Rect(7 + scrollPosition.x, startPointY, widthOfWindow, heightOfWindow);
        GUI.Box(editorBox, "");

        //Draw each COMMENT
        int lineNumber = 0;
        for(int i = 0; i < currentSelected.comments.Count; i++)
        {
            lineNumber += DrawCommentLine(currentSelected.comments[i], i + lineNumber, i);
        }

        //Delete empty comments from the array 
        foreach(Convo_Comment comment in currentSelected.comments)
        {
            for(int i = 0; i < comment.sentance.Count; i++)
            {
                if(comment.sentance[i] == null)
                {
                    comment.sentance.RemoveAt(i);
                    i--;
                }
            }
        }

        //Draw the Prefab box
        Rect objectFieldSizePos = new Rect();
        objectFieldSizePos.x = 14 + scrollPosition.x;
        objectFieldSizePos.y = startPointY + heightOfWindow - 25f;

        objectFieldSizePos.width = 80f;
        objectFieldSizePos.height = 17f;

        //Draw the isPlayer
        GUI.Label(objectFieldSizePos, "Player Node? ");

        objectFieldSizePos.x += 85f;
        objectFieldSizePos.width = 10f;
        currentSelected.isPlayer = EditorGUI.Toggle(objectFieldSizePos, currentSelected.isPlayer);

        //TODO: Draw the isPlayer and model information

    }

    int DrawCommentLine(Convo_Comment comment, int lineNumber, int commentNumber)
    {
        int lineNum = 0;

        //Set up the rect for positioning everything
        float startPointY = position.height - (position.height * 0.35f) + (20f * lineNumber) + scrollPosition.y;
        float newLineX = 15 + 85f + scrollPosition.x;
        float newLineY = 20f;

        Rect objectFieldSizePos = new Rect();
        objectFieldSizePos.x = 15 + scrollPosition.x;
        objectFieldSizePos.y = startPointY + 10f;
        objectFieldSizePos.width = 100f;
        objectFieldSizePos.height = 17f;

        //The actual start of the display
        GUI.Label(objectFieldSizePos, "Dialogue Line: ");

        objectFieldSizePos.width = 100f;
        objectFieldSizePos.x += 85f;
        
        //Draw the existing words
        for(int i = 0; i < comment.sentance.Count; i++)
        {
            //Check for "word" wrap
            if (ShouldWrap(objectFieldSizePos, 10f))
            {
                objectFieldSizePos.y += newLineY;
                objectFieldSizePos.x = newLineX;
                lineNum++;
            }

            comment.sentance[i] = (Convo_Word)EditorGUI.ObjectField(objectFieldSizePos, GUIContent.none, comment.sentance[i], typeof(Convo_Word), false);
            objectFieldSizePos.x += 100f;
        }

        //Check for wrap (again) so the temporary word doesn't run off the side of the window
        if (ShouldWrap(objectFieldSizePos, 10f))
        {
            objectFieldSizePos.y += newLineY;
            objectFieldSizePos.x = newLineX;
            lineNum++;
        }

        //Draw the slot to add a new word
        Convo_Word tempWord = null;
        tempWord = (Convo_Word)EditorGUI.ObjectField(objectFieldSizePos, GUIContent.none, tempWord, typeof(Convo_Word), false);

        if (tempWord != null)
            comment.sentance.Add(tempWord);

       
        //Draw the "extra" information - FORCE A NEW LINE
            //Delete Button
            //Reputation
            //Connecting node ID (for manual)
        //TODO: Check for new Lines

        objectFieldSizePos.y += newLineY;
        objectFieldSizePos.x = newLineX;
        lineNum++;

        //Draw Reputation Fields
        objectFieldSizePos.width = 120f;
        GUI.Label(objectFieldSizePos, "Reputation Needed: ");

        objectFieldSizePos.x += 120f;
        objectFieldSizePos.width = 30f;
        comment.repNeeded = EditorGUI.IntField(objectFieldSizePos, comment.repNeeded);


        //Draw the Connecting Node Fields, always check for new line 

        objectFieldSizePos.x += 35f;
        objectFieldSizePos.width = 130f;
        GUI.Label(objectFieldSizePos, "Connecting Node ID: ");

        objectFieldSizePos.x += 130f;
        objectFieldSizePos.width = 25f;

        int connectingID = EditorGUI.IntField(objectFieldSizePos, comment.outNode);
        comment.outNode = connectingID;

        //Draw the AUDIO
        objectFieldSizePos.x += 35f;
        objectFieldSizePos.width = 50f;

        EditorGUI.LabelField(objectFieldSizePos, "Audio: ");

        objectFieldSizePos.x += 45f;

        comment.audioFile = (AudioClip)EditorGUI.ObjectField(objectFieldSizePos, GUIContent.none, comment.audioFile, typeof(AudioClip), false);

        //Draw the ANIMATION
        objectFieldSizePos.x += 50;
        objectFieldSizePos.width = 65f;
        EditorGUI.LabelField(objectFieldSizePos, "Animation: ");

        objectFieldSizePos.x += 70f;
        comment.animationClip = (AnimationClip)EditorGUI.ObjectField(objectFieldSizePos, GUIContent.none, comment.animationClip, typeof(AnimationClip), false);

        //Draw the "Display Native" checkbox
        objectFieldSizePos.x += 65f;
        objectFieldSizePos.width = 95f;
        EditorGUI.LabelField(objectFieldSizePos, "Display Opposite?");

        objectFieldSizePos.x += 95;
        objectFieldSizePos.width = 20f;
        comment.displayNative = EditorGUI.Toggle(objectFieldSizePos, comment.displayNative);

        //Draw the DELETE button, always check for new line
        objectFieldSizePos.x += 20f;
        objectFieldSizePos.width = 100f;

        //Only draw the Delete Comment button if it is NOT the first comment
        if (commentNumber != 0)
        {
            GUI.backgroundColor = Color.red;

            if (GUI.Button(objectFieldSizePos, "Delete Line"))
            {
                currentSelected.comments.RemoveAt(commentNumber);
                return 0;   //If we are deleting the comment, we need to say we used 0 lines
            }

            //Reset the background color so we dont draw everything in red
            GUI.backgroundColor = Color.white;

        }

        return lineNum;

    }

    bool ShouldWrap(Rect currentPosition, float wrapBuffer)
    {
        if (currentPosition.x + currentPosition.width + wrapBuffer > position.width + scrollPosition.x)
            return true;
        else
            return false;
    }

    bool DeleteNode(Convo_Node toDelete)
    {
        int deleteID = toDelete.nodeID;

        //Find any node that uses this node 
        foreach (Convo_Node node in currentDialogue.nodes)
        {
            foreach (Convo_Comment comment in node.comments)
            {
                if (comment.outNode == deleteID)
                    comment.outNode = -1;
                if (comment.inNode == deleteID)
                    comment.inNode = -1;
            }
        }

        //DELETE NODE 
        int nodeIndex = GetIndexOfNode(deleteID);

        if (nodeIndex != -1)
        {
            currentDialogue.nodes.RemoveAt(nodeIndex);
            DrawLines();

            return true;
        }

        return false;
    }


    #region Drawing
    void DrawLines()
    {
        Handles.color = colors[3];

        if (isDrawingLine)
        {
            Vector2 newLine = Vector2.zero;
            newLine.x = currentSelected.gui.GetRect().x + currentSelected.gui.GetRect().width;
            newLine.y = handleRect.y + (handleRect.height / 2);
            DrawNodeLine3(new Vector2(newLine.x, newLine.y), Event.current.mousePosition);
            Repaint();
        }

        if (currentDialogue.nodes.Count != 0)
        {
            for (int nodeIndex = 0; nodeIndex < currentDialogue.nodes.Count; nodeIndex++)
            {
                for (int commentIndex = 0; commentIndex < currentDialogue.nodes[nodeIndex].comments.Count; commentIndex++)
                {
                    Rect guiRect = currentDialogue.nodes[nodeIndex].gui.GetRect();
                    guiRect.y -= 40;

                    //Should be two comments
                    if (currentDialogue.nodes[nodeIndex].comments[commentIndex].outNode > -1)
                    {
                        guiRect.y += 20f * commentIndex;    //This is the spacing of the arrows
                        DrawNodeLine(
                            guiRect,
                            FindByID(currentDialogue.nodes[nodeIndex].comments[commentIndex].outNode).gui.GetRect(),
                            guiRect);
                    }
                }
            }
        }
    }

    void DrawNodeLine(Rect start, Rect end, Rect sPos)
    {
        Vector3 startPos = new Vector3(start.x + start.width, start.y + (start.height / 2), 0);
        Vector3 endPos = new Vector3(end.x + 4, end.y + (end.height / 2), 0);

        float ab = Vector2.Distance(startPos, endPos);

        Vector3 startTan = startPos + Vector3.right * (ab / 3);
        Vector3 endTan = endPos + Vector3.left * (ab / 3);

        Handles.DrawBezier(startPos, endPos, startTan, endTan, colors[1], null, 3);


        //Draw arrow
        DrawArrow(startPos, startTan, endTan, endPos, sPos, start, true);
    }

    void DrawNodeLine3(Vector2 start, Vector2 end)
    {
        Vector3 startPos = new Vector3(start.x, start.y, 0);
        Vector3 endPos = new Vector3(end.x + 4, end.y, 0);

        float ab = Vector2.Distance(startPos, endPos);

        Vector3 startTan = startPos + Vector3.right * (ab / 3);
        Vector3 endTan = endPos + Vector3.left * (ab / 3);

        Handles.DrawBezier(startPos, endPos, startTan, endTan, colors[0], null, 5);

        DrawArrow(startPos, startTan, endTan, endPos, new Rect(0, 0, 0, 0), new Rect(start.x, start.y, 0, 0), false);
    }

    void DrawArrow(Vector3 startPos, Vector3 startTan, Vector3 endTan, Vector3 endPos, Rect sPos, Rect start, bool hasYpos)
    {
        Handles.BeginGUI();
        float ab = Vector2.Distance(startPos, endPos);
        if (ab < 25) return;

        float dist = 0.4f;

        Vector2 cen = Bezier3(startPos, startTan, endTan, endPos, dist);

        cen = Bezier3(startPos, startTan, endTan, endPos, dist);

        float rot = AngleBetweenVector2(cen, Bezier3(startPos, startTan, endTan, endPos, dist + 0.05f));

        Matrix4x4 matrixBackup = GUI.matrix;
        GUIUtility.RotateAroundPivot(rot + 90, new Vector2(cen.x, cen.y));

        GUI.color = colors[1];

        GUI.DrawTexture(new Rect(cen.x - 10, cen.y - 10, 20, 20), lineIcon, ScaleMode.StretchToFill);

        GUI.color = Color.white;
        GUI.matrix = matrixBackup;

        Handles.EndGUI();
    }

    void TryConnectNode(Vector2 mousePos)
    {
        //Debug.Log("Trying to find connection...");
        if (currentSelected == null) return;

        //Debug.Log("Made it!");
        if (FindOver(mousePos) != null)
        {
            currentSelected.comments[handleCommentNumber].SetOut(FindOver(mousePos).nodeID);
            Repaint();
            return;
        }
    }

    Vector3 Bezier3(Vector3 s, Vector3 st, Vector3 et, Vector3 e, float t)
    {
        return (((-s + 3 * (st - et) + e) * t + (3 * (s + et) - 6 * st)) * t + 3 * (st - s)) * t + s;
    }

    private float AngleBetweenVector2(Vector2 vec1, Vector2 vec2)
    {
        Vector2 diference = vec2 - vec1;
        float sign = (vec2.y < vec1.y) ? -1.0f : 1.0f;
        return Vector2.Angle(Vector2.right, diference) * sign;
    }
    #endregion

    #region IDs and Searching
    Convo_Node FindByID(int id)
    {
        foreach (Convo_Node node in currentDialogue.nodes)
        {
            if (node.nodeID == id)
                return node;
        }

        return null;
    }

    Convo_Node FindOver(Vector2 mousePos)
    {
        foreach (Convo_Node node in currentDialogue.nodes)
        {
            if (node.gui.GetRect().Contains(mousePos))
                return node;
        }
        return null;
    }

    Convo_Node FindByHandle(Rect handlePosition)
    {
        foreach(Convo_Node node in currentDialogue.nodes)
        {
            if (node.gui.GetRect().Contains(new Vector2(handlePosition.x - handlePosition.width / 2 - 10f, handlePosition.y)))
                return node;
        }

        return null;
    }

    


    private int GetUniqueID()
    {
        int tempID = 0;
        while (!SearchID(tempID))
            tempID++;

        return tempID;
    }

    bool SearchID(int id)
    {
        for (int i = 0; i < currentDialogue.nodes.Count; i++)
        {
            if (currentDialogue.nodes[i].nodeID == id) return false;
        }

        return true;
    }

    int GetIndexOfNode(int nodeID)
    {
        for (int i = 0; i < currentDialogue.nodes.Count; i++)
        {
            if (currentDialogue.nodes[i].nodeID == nodeID)
                return i;
        }

        return -1;
    }
    #endregion
}
