﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class Dialogue_Load : EditorWindow
{
    //string[] options = { "Rigidbody", "Box Collider", "Sphere Collider" };
    string[] options;
    int index = 0;

    //[MenuItem("Conversation Editor/Load Dialogue")]
    static void Init()
    {
        //OLD WAY - using JSON Files
        /*
        var window = GetWindow<Dialogue_Load>();
        window.position = new Rect(0, 0, 300, 80);
        window.Show();

        string directoryPath;
        List<string> fileNames;

        directoryPath = Application.dataPath + "/Dialogues/";
        fileNames = new List<string>(Directory.GetFiles(directoryPath));

        for(int i = 0; i < fileNames.Count; i++)
        {
            fileNames[i] = Path.GetFileName(fileNames[i]);
        }
        
        //Remove the metas
        for(int i = 0; i < fileNames.Count; i++)
        {
            if(fileNames[i].Split('.').Length > 2)
            {
                fileNames.RemoveAt(i);
                i--;
            }

        }

        window.options = fileNames.ToArray();
        */

    }

    private void OnGUI()
    {
        index = EditorGUI.Popup(new Rect(0, 5, position.width - 10f, 20), "Dialogue to Load", index, options);

        if(GUI.Button(new Rect(5, 30, position.width - 10f, position.height/2 - 30), "Load Dialogue"))
        {
            //OLD WAY - using JSON Files
            /*
            if (GameObject.Find("DialogueEditor") != null)
                GameObject.DestroyImmediate(GameObject.Find("DialogueEditor"));

            Convo_Dialogue currentDialogue = new Convo_Dialogue();
            EditorJsonUtility.FromJsonOverwrite(File.ReadAllText(Application.dataPath + "/Dialogues/" + options[index]), currentDialogue);

            GameObject newObject = new GameObject();
            newObject.transform.position = Vector3.zero;
            newObject.transform.rotation = Quaternion.identity;
            newObject.gameObject.name = "DialogueEditor";

            DialogueTester testerComp = newObject.AddComponent<DialogueTester>();
            testerComp.currentDialogue = currentDialogue;
            */



        }
    }

}
