﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterDollySwap : MonoBehaviour {

    [Tooltip("Put the starting scene Cinemachine camera into this variable")]
    public Cinemachine.CinemachineVirtualCamera currentCamera;


    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == "CameraChange")
        {
            if( other.GetComponent<Camera_ActivateDolly>().CheckCollision(transform))
            {
                currentCamera.m_Priority = 0;
                currentCamera = other.GetComponent<Camera_ActivateDolly>().cameraToActivate;
                currentCamera.m_Priority = 1;

            }
        }
    }

    public void ExternalSwap(Cinemachine.CinemachineVirtualCamera cameraToActivate)
    {
        currentCamera.m_Priority = 0;
        currentCamera = cameraToActivate;
        currentCamera.m_Priority = 1;
    }
}
