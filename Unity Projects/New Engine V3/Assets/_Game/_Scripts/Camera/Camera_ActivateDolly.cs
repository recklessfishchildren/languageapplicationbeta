﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CollisionAxis
{
    X,
    Z
}


public enum CollideOn
{
    Positive,
    Negative
}



public class Camera_ActivateDolly : MonoBehaviour {

    public Cinemachine.CinemachineVirtualCamera cameraToActivate;

    public CollisionAxis collisionAxis;
    public CollideOn collideOn;

    bool triggerFromRight = true;



    public bool CheckCollision(Transform playerTransform)
    {
        if(collisionAxis == CollisionAxis.X)
        {
            //If we are using X, and want to collide in the positive direction
            if (collideOn == CollideOn.Positive)
            {
                if (playerTransform.position.x > transform.position.x)
                    return true;
                else
                    return false;
            }
            else
            {
                if (playerTransform.position.x < transform.position.x)
                    return true;
                else
                    return false;
            }
        }
        else
        {
            if(collideOn == CollideOn.Positive)
            {
                if (playerTransform.position.z > transform.position.z)
                    return true;
                else
                    return false;
            }
            else
            {
                if (playerTransform.position.z < transform.position.z)
                    return true;
                else
                    return false;
            }
        }


        
        
    }
}
