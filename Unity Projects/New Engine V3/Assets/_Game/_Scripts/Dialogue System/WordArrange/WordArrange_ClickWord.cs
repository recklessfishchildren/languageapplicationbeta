﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordArrange_ClickWord : MonoBehaviour
{
    bool arranged = false;
    Button thisButton;
    ColorBlock correctBlock;

    void Start()
    {
        thisButton = GetComponent<Button>();
        correctBlock = thisButton.colors;
        correctBlock.normalColor = new Color(153 / 255f, 203 / 255f, 60 / 255f);
    }
    public void _Click()
    {
        arranged = !arranged;

        if (arranged)
            transform.SetParent(Testing_DialogueUI.instance.arranged.transform);
        else
        {
            thisButton.colors = correctBlock;

            transform.SetParent(Testing_DialogueUI.instance.notArranged.transform);
        }
    }
}
