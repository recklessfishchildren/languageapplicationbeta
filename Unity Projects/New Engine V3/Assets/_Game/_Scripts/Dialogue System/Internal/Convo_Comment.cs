﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[Serializable]
public class Convo_Comment {

    public string commentData;
    public List<Convo_Word> sentance;

    public int inNode;
    public int outNode;
    public int repNeeded;

    public bool displayNative;

    public AudioClip audioFile;
    public AnimationClip animationClip;

    public Convo_Comment()
    {
        inNode = -1;
        outNode = -1;
        repNeeded = 0;
        sentance = new List<Convo_Word>();
    }

    public void SetOut(int node)
    {
        outNode = node;
    }

}
