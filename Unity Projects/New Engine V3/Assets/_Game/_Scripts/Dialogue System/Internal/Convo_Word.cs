﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[Serializable, CreateAssetMenu(fileName = "Word", menuName = "Dialogue/Word", order = 1)]
public class Convo_Word : ScriptableObject {

    public string native;
    public bool nativeNoSpace;
    public string foreign;
    public bool foreignNoSpace;

    public string tooltipText;

    public bool learned;
    public int correctInARow;
    public int incorrectInARow;



    public void MarkCorrect()
    {
        correctInARow++;
        incorrectInARow = 0;

        if (correctInARow >= 5)
        {
            learned = true;
        }
    }

    public void MarkIncorrect()
    {
        correctInARow = 0;
        incorrectInARow++;
        if(incorrectInARow >= 3)
        {
            learned = false;
        }
    }
}
