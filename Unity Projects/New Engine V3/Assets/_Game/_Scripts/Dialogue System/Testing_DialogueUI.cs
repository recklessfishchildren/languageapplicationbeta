﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Testing_DialogueUI : MonoBehaviour {

    public static Testing_DialogueUI instance;

    public GameObject canvasObject;

    [Header("Bottom part of the dialogue")]
    [Tooltip("Will be disabled when PC is talking")]
    public GameObject npcPanel;
    [Tooltip("Where the name for NPC or PC will display")]
    public Text nameText;
    [Tooltip("Where the NPC text will show")]
    public Text dialogueText;

    [Header("Top Dialogue")]
    [Tooltip("Where the diaglogue history will appear. Should be scrollable and such")]
    public Text ghostText;

    [Header("Player Responses")]
    [Tooltip("Will be disabled when NPC is talking")]
    public GameObject playerResponsePanel;
    [Tooltip("The button that shows the players responses")]
    public GameObject buttonObject;
    [Tooltip("The panel the buttons are childed to. Should have a layout group")]
    public GameObject playerButtonPanel;

    [Header("Word Arrange UI")]
    [Tooltip("The top level object that contains all the WordArrange elements. Gets enabled/disabled.")]
    public GameObject wordArrangeObject;
    [Tooltip("The button for the words in the word arrange portion of the game")]
    public GameObject wordArrange_buttonTemplate;
    [Tooltip("The panel where the not arranged buttons will appear.")]
    public GameObject notArranged;
    [Tooltip("The panel where the arranged buttons will appear")]
    public GameObject arranged;
    [Tooltip("The panel under all the other objects. This makes sure dragged words appear above everything")]
    public GameObject dragParent;
    [Tooltip("Where to show the sentance the player is trying to create")]
    public Text txt_correctSentance;
    public float buffer;

    private void Start()
    {
        instance = this;
    }

    public void ShowDialogue()
    {
        canvasObject.SetActive(true);
    }

    public void HideDialogue()
    {
        canvasObject.SetActive(false);
    }

    public void ShowNPCBox()
    {
        npcPanel.SetActive(true);
    }

    public void HideNPCBox()
    {
        npcPanel.SetActive(false);
    }

    public void ShowPlayerBox()
    {
        playerResponsePanel.SetActive(true);
    }

    public void HidePlayerBox()
    {
        playerResponsePanel.SetActive(false);
    }

    public void CleanupPlayerBox()
    {
        foreach (Transform child in playerButtonPanel.transform)
            Destroy(child.gameObject);
    }



    public void HideWordArrange()
    {
        wordArrangeObject.SetActive(false);
    }

    public void ShowWordArrange()
    {
        wordArrangeObject.SetActive(true);
    }

    public void UpdateWordArrangeCorrectSentance(string text)
    {
        txt_correctSentance.text = text;
    }

    public void WordArrangeCleanUp()
    {
        foreach (Transform child in arranged.transform)
            Destroy(child.gameObject);

        foreach (Transform child in notArranged.transform)
            Destroy(child.gameObject);
    }

    public void UpdateName(string name)
    {
        nameText.text = name;
    }

    public void UpdateDialogueText(string text)
    {
        dialogueText.text = text;
    }

    public Button CreateResponse()
    {
        return Instantiate(buttonObject, playerButtonPanel.transform).GetComponent<Button>();
    }

    public void AddToGhost(string text, bool player)
    {
        if (player)
            ghostText.text += "=> " + text + "\n";
        else
            ghostText.text += text + "\n";
    }

    public void ResetGhost()
    {
        ghostText.text = "";
    }

}
